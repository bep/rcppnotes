# Rcpp/Armadillo Package development
Benno Pütz

## Rcpp Info

## Armadillo
- [Armadillo homepage](http://arma.sourceforge.net/)
- [Intro talk](https://scholar.princeton.edu/sites/default/files/q-aps/files/slides_day4_am.pdf)

## Packaging instructions
- [Documenting Rcpp packages](https://gallery.rcpp.org/articles/documenting-rcpp-packages/) general instructions and roxygen 

## C++
### `std` classses
-  [`ìfstream`](http://www.cplusplus.com/reference/fstream/ifstream/?kw=ifstream)
- [`string`](http://www.cplusplus.com/reference/string/string/?kw=string)
- [`vector`](http://www.cplusplus.com/reference/vector/vector/?kw=vector)

### References
#### Rcpp
- [Quickref (cheatsheet)](https://cran.r-project.org/web/packages/Rcpp/vignettes/Rcpp-quickref.pdf)
- [Rcpp for everyone](https://teuder.github.io/rcpp4everyone_en/)
	- [Vector](https://teuder.github.io/rcpp4everyone_en/080_vector.html)
	- [Matrix](https://teuder.github.io/rcpp4everyone_en/100_matrix.html)
- [Rcpp FAQ](https://cran.r-project.org/web/packages/Rcpp/vignettes/Rcpp-FAQ.pdf)
- [Rcpp notes 1.0.4](https://www.r-bloggers.com/rcpp-1-0-4-lots-of-goodies/) (2020-03-17)

#### C++
- [cppreference](https://en.cppreference.com/w/)
- [cplusplus](http://www.cplusplus.com/)
- [LearnCpp](https://www.learncpp.com/)
- [C++ biggest changes](https://smartbear.com/blog/develop/the-biggest-changes-in-c11-and-why-you-should-care/)

### Documentation
- Compilers
	- `clang`
		- [Attribute Ref](https://bcain-llvm.readthedocs.io/projects/clang/en/release_50/AttributeReference/)
	- GNU `gcc`/`g++`
		- [GNU `g++`/`gcc`](https://gcc.gnu.org/onlinedocs/gcc/index.html#Top)
	- [`gcc` vs. `clang`](https://medium.com/@alitech_2017/gcc-vs-clang-llvm-an-in-depth-comparison-of-c-c-compilers-899ede2be378)
- Roxygen
- [Doxygen](https://www.doxygen.nl)
	- [Manual]()
	- no direct conversion **Doxygen** ↔︎ **Roxygen** (some `roxygen` keywords are not recognized by `doxygen`) 

### Miscellaneous Topics
#### template function alternatives / workaround
- [return macros (2017)](https://gallery.rcpp.org/articles/rcpp-return-macros/)
- [Dynamic Wrapping and Recursion (2013)](https://gallery.rcpp.org/articles/rcpp-wrap-and-recurse/)

#### Multi-threading
- [Async threads with `std::sasync`](https://thispointer.com/c11-multithreading-part-9-stdasync-tutorial-example/) for optimizing `read_buffer`
- [Simple MT](http://www.devx.com/SpecialReports/Article/38883)

#### Others

- [Call R function from Rcpp](https://teuder.github.io/rcpp4everyone_en/230_R_function.html) (→ `write.table()`?)
- [I/O tips](http://www.augustcouncil.com/~tgibson/tutorial/iotips.html)
- [Timing: std::chrono](https://www.geeksforgeeks.org/measure-execution-time-function-cpp/)
- [NumericVector vs std::vector](https://stackoverflow.com/questions/41602024/should-i-prefer-rcppnumericvector-over-stdvector)
- [Using Rcout with Rcpp / RcppArmadillo](http://dirk.eddelbuettel.com/blog/2012/02/18/)

#### Static/dynamic libraries
- [Stat/Dyn Lib on Mac OSX](http://nickdesaulniers.github.io/blog/2016/11/20/static-and-dynamic-libraries/)
- [shared lib with clang on OSX](https://stackoverflow.com/questions/21907504/how-to-compile-shared-lib-with-clang-on-osx)

#### High-level Rcpp: Sugar
- [Rcpp sugar](https://dirk.eddelbuettel.com/code/rcpp/Rcpp-sugar.pdf)
- [RcppHoney](https://github.com/dcdillon/RcppHoney)